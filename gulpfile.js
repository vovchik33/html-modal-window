var gulp = require('gulp'),
    minifyCSS = require('gulp-minify-css'),
    concatCSS = require('gulp-concat-css'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename');
    
gulp.task('updateHtml', function() {
    gulp.src('src/*.html')
        .pipe(gulp.dest('build/'))
        .pipe(notify('HTML is updated!'));
    console.log('HTML is updated!');
});
gulp.task('updateCss', function() {
    gulp.src('src/css/*.css')
        .pipe(concatCSS('bundle.css'))
        .pipe(minifyCSS())
        .pipe(rename('bundle.min.css'))
        .pipe(gulp.dest('build/css/'))
        .pipe(notify('CSS is updated!'));
    console.log('CSS is updated!');
});
gulp.task('updateJs', function() {
    gulp.src('src/js/*.js')
        .pipe(gulp.dest('build/js/'))
        .pipe(notify('JavaScript is updated!'));
    console.log('JavaScript is updated!');
});
gulp.task('watch', function() {
    gulp.watch('src/index.html', ['updateHtml']);
    gulp.watch('src/css/*.css', ['updateCss']);
    gulp.watch('src/js/*.js', ['updateJs']);
});
gulp.task('default', ['watch']);